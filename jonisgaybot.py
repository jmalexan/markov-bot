import requests
from pprint import pprint
import json
from websocket import create_connection
import asyncio
import threading
import markov
from globvar import *
from discordapi import *
from trollpingbot import *

if __name__ == "__main__":
    # Handshake
    gateWay = "wss://gateway.discord.gg/"
    handshakePayload = """{
        "op": 2,
        "d": {
        "token": "MTc0NjU1NDY4MjczMjcwNzg2.CgPpwg.68OYfGtdWLZH3D0kW04bXOAmxms",
        "properties": {},
        "compress": false,
        "large_threshold": 250
        }
    }"""
    ws = create_connection(gateWay)
    ws.send(handshakePayload)
    readyPayload = json.loads(ws.recv())

    # Start heartbeat
    heartbeatThread = Heartbeat(ws, readyPayload['d']['heartbeat_interval']/1000)
    heartbeatThread.start()

    trollPingThread = TrollPing()
    trollPingThread.start()

    # Start receiving messages from gateway
    while True:
        recv_raw = ws.recv()
        if recv_raw != '':
##            print(recv_raw.encode("utf-8"))
##            print("--------------------------------------------------------------------------------------------------------------------------------------------")
            receive = json.loads(recv_raw)
            heartbeatThread.last_seq = receive['s']  # Update hearbeat sequence number
            
            print(json.dumps(receive, indent=2))

            if receive['t'] == 'MESSAGE_CREATE':
               content = receive['d']['content']
               channelID = receive['d']['channel_id']
               contentSplit = content.split(" ")

               # Jonisgay 
               if contentSplit[0] == "!jonisgay":  
                   sendMessage("Jon is g\u00C6y af!", channelID)
                   
               # Shit x says 
               if len(contentSplit) > 2 and contentSplit[0].lower() == "shit" and contentSplit[2].lower() == "says":
                   username = contentSplit[1]
                   messageList = getMessageJson(channelID)
                   if messageList == "ERROR":
                       sendMessage("PLEASE STOP SENDING SHIT YOU'RE GOING TO MAKE ME LOSE MY DEVKEY", channelID)
                   messageString = parseMessageJson(messageList, username)
                   if not messageString:
                       sendMessage(username + " has never said anything ever!", channelID)
                       continue
                   s = markov.split_string(messageString)
                   m = markov.markov_chain(s)
                   mg = markov.markov_generate(m)
                   sendMessage(username + ": " + mg, channelID)

