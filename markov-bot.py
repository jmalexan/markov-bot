import discord
import asyncio

client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    for c in client.get_all_channels():
        if c.type == discord.ChannelType.text:
            async for message in client.logs_from(c, limit=100000):
                try:
                    print(message.content)
                except UnicodeEncodeError:
                    print("lolrekt")

@client.event
async def on_message(message):
    if message.content.startswith('!test'):
        counter = 0
        tmp = await client.send_message(message.channel, 'Calculating messages...')
        async for log in client.logs_from(message.channel, limit=100):
            if log.author == message.author:
                counter += 1

        await client.edit_message(tmp, 'You have {} messages.'.format(counter))
    elif message.content.startswith('!sleep'):
        await asyncio.sleep(5)
        await client.send_message(message.channel, 'Done sleeping')

client.run("MTc0NjU0OTE3NzI4OTI3NzQ0.CgGEpA.PG0c65lxRpPTvMN715sz54DdaSg")
